package com.rajivprab.poker.PlayerImpl;

import com.rajivprab.poker.GameClient.Move;
import com.rajivprab.poker.GameClient.MovesThisHand;
import com.rajivprab.poker.GameClient.Player;
import com.rajivprab.poker.GameClient.Round.Street;
import com.rajivprab.poker.GameClient.Utils.StrategyUtils;

/**
 * PreFlop: Use slansky hand-rank to determine whether to raise/call/fold
 * PostFlop: Only raises/calls with good hands. Check/Folds everything else
 *
 * Created by rajivprab on 1/16/17.
 */
public class TightPlayer extends Player {
    public TightPlayer(String name, int buyIn) {
        super(name, buyIn);
    }

    @Override
    public Move computeMove(MovesThisHand movesThisHand, Street currentStreet) {
        if (currentStreet.equals(Street.PRE_FLOP)) {
            return StrategyUtils.getTightPreFlopMove(this, movesThisHand);
        } else {
            return StrategyUtils.getTightPostFlopMove(this, movesThisHand, currentStreet);
        }
    }

    @Override
    public void processRound(MovesThisHand moves) {}
}

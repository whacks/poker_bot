package com.rajivprab.poker.PlayerImpl;

import com.rajivprab.poker.GameClient.Move;
import com.rajivprab.poker.GameClient.Move.Decision;
import com.rajivprab.poker.GameClient.MovesThisHand;
import com.rajivprab.poker.GameClient.Player;
import com.rajivprab.poker.GameClient.Round.Street;
import com.rajivprab.poker.GameClient.Utils.PotUtils;

import java.util.Map;
import java.util.Random;

/**
 * Randomly raises/calls/checks/folds. Follows all rules.
 *
 * Created by rprabhakar on 5/26/15.
 */
public class RandomPlayer extends Player {
    private Random rng = new Random();

    public RandomPlayer(String name, int buyIn) {
        super(name, buyIn);
    }

    @Override
    public Move computeMove(MovesThisHand movesByStreet, Street currentStreet) {
        Map<String, Integer> playerBets = PotUtils.getTotalPlayerBets(movesByStreet.getMovesForStreet(currentStreet));
        int nextHighestBet = PotUtils.getOtherPlayersHighestBet(playerBets, getName());
        int myBet = PotUtils.getPlayerAggregateBet(playerBets, getName());
        int amountToCall = movesByStreet.amountToCall(currentStreet, this);

        int rand = rng.nextInt(100);
        if (rand < 20) {
            return new Move(this, Decision.RAISE, amountToCall + movesByStreet.getPotValue() / 2)
                    .truncateBetIfShort(amountToCall);
        } else if (rand < 80) {
            return amountToCall > 0 ?
                    new Move(this, Decision.CALL, amountToCall) :
                    new Move(this, Decision.CHECK);
        } else {
            return amountToCall > 0 ?
                    new Move(this, Decision.FOLD) :
                    new Move(this, Decision.CHECK);
        }
    }

    @Override
    public void processRound(MovesThisHand moves) {}
}

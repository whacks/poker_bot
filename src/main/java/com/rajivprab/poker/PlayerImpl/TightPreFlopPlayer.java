package com.rajivprab.poker.PlayerImpl;

import com.rajivprab.poker.GameClient.Move;
import com.rajivprab.poker.GameClient.Move.Decision;
import com.rajivprab.poker.GameClient.MovesThisHand;
import com.rajivprab.poker.GameClient.Player;
import com.rajivprab.poker.GameClient.Round.Street;
import com.rajivprab.poker.GameClient.Utils.StrategyUtils;

/**
 * Tight aggressive pre-flop
 * Calling station post-flop
 *
 * Created by rprabhakar on 6/2/15.
 */
public class TightPreFlopPlayer extends Player {
    public TightPreFlopPlayer(String name, int buyIn) {
        super(name, buyIn);
    }

    @Override
    public Move computeMove(MovesThisHand movesThisHand, Street currentStreet) {
        if (currentStreet.equals(Street.PRE_FLOP)) {
            return StrategyUtils.getTightPreFlopMove(this, movesThisHand);
        } else {
            return StrategyUtils.getTightPostFlopMove(this, movesThisHand, currentStreet);
        }
    }

    private Move getPostFlopMove(MovesThisHand movesThisHand, Street currentStreet) {
        int amountToCall = movesThisHand.amountToCall(currentStreet, getName());
        return amountToCall <= 0 ? new Move(this, Decision.CHECK) : new Move(this, Decision.CALL, amountToCall);
    }

    @Override
    public void processRound(MovesThisHand moves) {}
}

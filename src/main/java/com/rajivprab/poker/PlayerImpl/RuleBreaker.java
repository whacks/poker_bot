package com.rajivprab.poker.PlayerImpl;

import com.rajivprab.poker.GameClient.Move;
import com.rajivprab.poker.GameClient.Move.Decision;
import com.rajivprab.poker.GameClient.MovesThisHand;
import com.rajivprab.poker.GameClient.Player;
import com.rajivprab.poker.GameClient.Round.Street;
import org.junit.Assert;
import org.rajivprab.cava.ThreadUtilc;

import java.util.Random;

/**
 * Player who breaks all possible rules and game states
 * In order to check that GameClient handles them all adequately
 *
 * Created by rprabhakar on 5/26/15.
 */
public class RuleBreaker extends Player {
    private Random rng = new Random();

    public RuleBreaker(String name, int buyIn) {
        super(name, buyIn);
    }

    @Override
    public Move computeMove(MovesThisHand movesByStreet, Street currentStreet) {
        int rand = rng.nextInt(100);
        if (rand < 2) {
            ThreadUtilc.sleep(TIMEOUT_MS + 1);
            return new Move(this, Decision.FOLD);
        } else if (rand < 10) {
            movesByStreet.getMovesForStreet(Street.PRE_FLOP).add(new Move(this, Decision.FOLD));
            System.out.println("RuleBreaker just got done modifying game state!! This should never get printed");
            ThreadUtilc.sleep(1000 * 1000);
            return new Move(this, Decision.FOLD);
        } else if (rand < 20) {
            Assert.fail("Throwing assertion because I can");
            return new Move(this, Decision.FOLD, 0);
        } else if (rand < 30) {
            return new Move(this, Decision.FOLD);
        } else if (rand < 40) {
            return new Move(this, Decision.RAISE, getStack() + 1);
        } else if (rand < 60) {
            return new Move(this, Decision.CALL, getStack() + 1);
        } else if (rand < 80) {
            return new Move(this, Decision.CALL, 2);
        } else {
            return new Move(this, Decision.CHECK);
        }
    }

    @Override
    public void processRound(MovesThisHand moves) {}
}

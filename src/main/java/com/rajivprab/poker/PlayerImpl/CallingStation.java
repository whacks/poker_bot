package com.rajivprab.poker.PlayerImpl;

import com.rajivprab.poker.GameClient.Move;
import com.rajivprab.poker.GameClient.Move.Decision;
import com.rajivprab.poker.GameClient.MovesThisHand;
import com.rajivprab.poker.GameClient.Player;
import com.rajivprab.poker.GameClient.Round.Street;
import com.rajivprab.poker.GameClient.Utils.PotUtils;

import java.util.Map;

/**
 * Player implementation that simply calls or checks forever
 *
 * Created by rprabhakar on 5/25/15.
 */
public class CallingStation extends Player {
    public CallingStation(String name, Integer buyIn) {
        super(name, buyIn);
    }

    @Override
    public Move computeMove(MovesThisHand movesByStreet, Street currentStreet) {
        Map<String, Integer> playerBets = PotUtils.getTotalPlayerBets(movesByStreet.getMovesForStreet(currentStreet));
        int nextHighestBet = PotUtils.getOtherPlayersHighestBet(playerBets, getName());
        int myBet = PotUtils.getPlayerAggregateBet(playerBets, getName());

        if (myBet < nextHighestBet) {
            return new Move(this, Decision.CALL, nextHighestBet - myBet);
        } else {
            return new Move(this, Decision.CHECK);
        }
    }

    @Override
    public void processRound(MovesThisHand moves) {}
}

package com.rajivprab.poker.GameClient;

import com.google.common.collect.Sets;

import java.util.Collection;

/**
 * Collection of cards being held by a player
 * Should include community cards as well, to properly compute com.rajivprab.poker.GameClient.HandStrength
 *
 * Created by rprabhakar on 4/5/15.
 */
public class Hand {
    private final Collection<Card> myCards;
    private HandStrength handStrength;

    private Hand(Hand ref) {
        this.myCards = ref.getCards();
        this.handStrength = ref.handStrength;
    }

    public Hand(Collection<Card> cards) {
        this.myCards = Sets.newHashSet(cards);
        handStrength = new HandStrength(cards);
    }

    public Hand addCard(Card newCard) {
        this.myCards.add(newCard);
        this.handStrength = new HandStrength(getCards());
        return this;
    }

    public Collection<Card> getCards() {
        return Sets.newHashSet(myCards);
    }

    public HandStrength getHandStrength() {
        return handStrength;
    }

    public Hand copyHand() {
        return new Hand(this);
    }
}

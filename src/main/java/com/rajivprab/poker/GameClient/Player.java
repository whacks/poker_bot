package com.rajivprab.poker.GameClient;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.rajivprab.poker.GameClient.Move.Decision;
import com.rajivprab.poker.GameClient.Round.Street;
import com.rajivprab.poker.GameClient.Utils.PotUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Representation of a single player.
 * Players should/do not have access to the Player-object for other players.
 * <p>
 * All player-implementations should be defined only outside this package,
 * to ensure that malicious implementations cannot call the package-private methods below that alter game state
 * <p>
 * Created by rprabhakar on 4/5/15.
 */
public abstract class Player {
    protected static final long TIMEOUT_MS = 100;

    private final String name;

    private Table table;
    private int stack;
    private Collection<Card> holeCards;

    public Player(String name, int buyIn) {
        this.name = name;
        this.stack = buyIn;
    }

    // Methods that are accessible to both GameClient (perfectly trusted)
    // and implementing player-object (trusted only with own info)
    // Should not change game-state in any way, nor reveal hidden info about other players, or their Player object
    public int getStack() {
        return stack;
    }

    public String getName() {
        return name;
    }

    public HandStrength getHandStrength(Collection<Card> communityCards) {
        Validatec.notEmpty(communityCards);
        Validatec.notEmpty(holeCards);
        return new HandStrength(Stream.concat(communityCards.stream(), holeCards.stream()).collect(Collectors.toList()));
    }

    public Collection<Card> getHoleCards() {
        return holeCards;
    }

    public List<Entry<String, Integer>> getPlayerOrderStacks() {
        return table.getOrderedPlayers().stream()
                    .map(player -> Maps.immutableEntry(player.getName(), player.getStack()))
                    .collect(Collectors.toList());
    }

    public abstract Move computeMove(MovesThisHand movesByStreet, Street currentStreet);

    public abstract void processRound(MovesThisHand moves);

    // Methods that are accessible only by GameClient, and not PlayerImpl
    void notifyAction(MovesThisHand movesByStreet) {
        try {
            Date startTime = new Date();
            processRound(movesByStreet);
            Date endTime = new Date();
            if (endTime.getTime() - startTime.getTime() > TIMEOUT_MS) {
                throw new PlayerIsTakingTooLongException(name, endTime.getTime() - startTime.getTime());
            }
        } catch (Throwable e) {
            penalize(stack / 10, getErrorHeader());
            PotUtils.printErrors(ExceptionUtils.getStackTrace(e));
        }
    }

    Player withTable(Table table) {
        this.table = table;
        return this;
    }

    Player dealCards(Card holeCard1, Card holeCard2) {
        holeCards = ImmutableList.of(holeCard1, holeCard2);
        return this;
    }

    Move getMove(MovesThisHand movesByStreet, Street currentStreet) {
        Move move;
        try {
            Date startTime = new Date();
            move = computeMove(movesByStreet, currentStreet);
            Date endTime = new Date();
            if (endTime.getTime() - startTime.getTime() > TIMEOUT_MS) {
                throw new PlayerIsTakingTooLongException(name, endTime.getTime() - startTime.getTime());
            }
            checkForIllegalMove(movesByStreet.getMovesForStreet(currentStreet), move);
        } catch (Throwable e) {
            move = penalize(stack / 10, getErrorHeader());
            PotUtils.printErrors(ExceptionUtils.getStackTrace(e));
        }
        stack -= move.getAmount();
        return move;
    }

    Move postBlind(int blind) {
        Validatec.greaterThan(stack, blind - 1);
        Move move = new Move(this, Decision.RAISE, blind);
        stack -= blind;
        return move;
    }

    void collectWinnings(int pot) {
        stack += pot;
    }

    Player clearHand() {
        holeCards = null;
        return this;
    }

    // ---------- Private utilities for internal implementation only

    private Move penalize(int penalty, String message) {
        System.out.println(message);
        stack -= penalty;
        return new Move(this, Decision.FOLD);
    }

    private void checkForIllegalMove(List<Move> movesThisStreet, Move move) throws IllegalPlayerMoveException {
        Decision curDecision = move.getDecision();
        if (stack < move.getAmount() && curDecision == Decision.RAISE) {
            throw new IllegalPlayerMoveException(name, "Cannot raise unless you have the stack for it\n" +
                                                 "Move: " + move + "\nAction: " + movesThisStreet);
        }

        Map<String, Integer> playerBets = PotUtils.getTotalPlayerBets(movesThisStreet);
        int nextHighestBet = PotUtils.getOtherPlayersHighestBet(playerBets, name);
        int myTotalBet = PotUtils.getPlayerAggregateBet(playerBets, name) + move.getAmount();

        switch (curDecision) {
            case CALL:
                if (myTotalBet != nextHighestBet) {
                    throw new IllegalPlayerMoveException(name, "Check/call must match other person's bet.\n" +
                            "Move: " + move + "\nAction: " + movesThisStreet);
                }
                break;
            case RAISE:
                if (myTotalBet <= nextHighestBet) {
                    throw new IllegalPlayerMoveException(name, "Raise must exceed other person's bet\n" +
                            "Move: " + move + "\nAction: " + movesThisStreet);
                }
                break;
            default:
        }
    }

    private String getErrorHeader() {
        return "Exception/Error hit by: " + getName() + " of class: " + this.getClass();
    }

    // Exception class definitions
    public static class PlayerIsBrokeException extends Exception {
        private final String playerName;

        public PlayerIsBrokeException(String playerName) {
            this.playerName = playerName;
        }

        public String getPlayerName() {
            return playerName;
        }
    }

    public static class IllegalPlayerMoveException extends Exception {
        private final String playerName;

        public IllegalPlayerMoveException(String playerName, String message) {
            super(playerName + " made an illegal move: " + message);
            this.playerName = playerName;
        }

        public String getPlayerName() {
            return playerName;
        }
    }

    public static class PlayerIsTakingTooLongException extends Exception {
        private final String playerName;
        private final long timeTakenMs;

        public PlayerIsTakingTooLongException(String playerName, long timeTakenMs) {
            super(playerName + " took too much time(ms): " + timeTakenMs);
            this.playerName = playerName;
            this.timeTakenMs = timeTakenMs;
        }

        public String getPlayerName() {
            return playerName;
        }
    }

    // Generic object methods
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        Player ref = (Player) obj;
        return name.equals(ref.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}

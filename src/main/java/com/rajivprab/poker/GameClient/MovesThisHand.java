package com.rajivprab.poker.GameClient;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.rajivprab.poker.GameClient.Move.Decision;
import com.rajivprab.poker.GameClient.Round.Street;
import com.rajivprab.poker.GameClient.Utils.PotUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Structure tracking, providing and protecting data,
 * for all moves made this hand (ie, all 5 streets)
 *
 * Created by rprabhakar on 5/25/15.
 */
public class MovesThisHand {
    private final List<Card> communityCards;
    private final Map<Street, List<Move>> movesByStreet = ImmutableMap.of(
            Street.PRE_FLOP, Lists.newArrayList(),
            Street.FLOP, Lists.newArrayList(),
            Street.TURN, Lists.newArrayList(),
            Street.RIVER, Lists.newArrayList());

    public MovesThisHand(List<Card> communityCards) {
        this.communityCards = communityCards;
    }

    // Returns read-only view of all moves made on that street, together with the player-name who made that move
    // Underlying list can and will be updated
    public List<Move> getMovesForStreet(Street street) {
        return Collections.unmodifiableList(movesByStreet.get(street));
    }

    // Returns read-only view of all community-cards, which can and will be updated as the Hand progresses
    public List<Card> getCommunityCards() {
        return Collections.unmodifiableList(communityCards);
    }

    public Set<String> getFoldedPlayers(Street street) {
        return movesByStreet.get(street).stream()
                            .filter(move -> move.getDecision().equals(Decision.FOLD))
                            .map(Move::getPlayerName)
                            .collect(Collectors.toSet());
    }

    public Set<String> getActivePlayers(Street street) {
        Set<String> activePlayers = movesByStreet.get(street).stream().map(Move::getPlayerName).collect(Collectors.toSet());
        activePlayers.removeAll(getFoldedPlayers(street));
        return activePlayers;
    }

    public void printAction() {
        Arrays.stream(Street.values()).forEach(s -> PotUtils.printRoundInfo(s + ": " + movesByStreet.get(s)));
    }

    public int getBigBlind() {
        return getMovesForStreet(Street.PRE_FLOP).get(1).getAmount();
    }

    public int amountToCall(Street street, Player player) {
        return amountToCall(street, player.getName());
    }

    public int amountToCall(Street street, String playerName) {
        Map<String, Integer> playerBets = PotUtils.getTotalPlayerBets(getMovesForStreet(street));
        int nextHighestBet = PotUtils.getOtherPlayersHighestBet(playerBets, playerName);
        int myTotalBet = PotUtils.getPlayerAggregateBet(playerBets, playerName);
        return nextHighestBet - myTotalBet;
    }

    public int getPotValue() {
        return Arrays.stream(Street.values())
                     .map(this::getMovesForStreet)
                     .flatMap(Collection::stream)
                     .mapToInt(Move::getAmount)
                     .sum();
    }

    // -------- Package-private: Player-Implementations not allowed to access this

    MovesThisHand addMove(Move move, Street street) {
        movesByStreet.get(street).add(move);
        return this;
    }

    MovesThisHand withCommunityCard(Card... communityCard) {
        Arrays.stream(communityCard).forEach(communityCards::add);
        return this;
    }

    @Override
    public String toString() {
        return Joiner.on("\n").join(movesByStreet.entrySet());
    }
}

package com.rajivprab.poker.GameClient;

/**
 * Representation of a single, immutable card
 *
 * Created by rprabhakar on 4/5/15.
 */
public class Card implements Comparable<Card> {
    @Override
    public int compareTo(Card ref) {
        if (!myRank.equals(ref.getRank())) {
            return myRank.compareTo(ref.getRank());
        }

        return mySuit.compareTo(ref.getSuit());
    }

    public enum Suit { CLUBS, DIAMONDS, HEARTS, SPADES }
    public enum Rank { TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE }

    private final Suit mySuit;
    private final Rank myRank;

    public Card(Suit suit, Rank rank) {
        this.mySuit = suit;
        this.myRank = rank;
    }

    public Suit getSuit() {
        return mySuit;
    }

    public Rank getRank() {
        return myRank;
    }

    @Override
    public boolean equals(Object ref) {
        if (ref == null) { return false; }
        if (ref == this) { return true; }
        if (!ref.getClass().equals(this.getClass())) { return false; }

        Card refCard = (Card) ref;
        return refCard.getRank().equals(getRank()) &&
                refCard.getSuit().equals(getSuit());
    }

    @Override
    public int hashCode() {
        return getSuit().ordinal() * 15 + getRank().ordinal();
    }

    @Override
    public String toString() {
        return myRank.toString() + " of " + mySuit;
    }
}

package com.rajivprab.poker.GameClient;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.rajivprab.cava.Validatec;

import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * Representation of table layout,
 * with dealer info and seating arrangements
 *
 * Created by rprabhakar on 5/4/15.
 */
public class Table {
    private ImmutableList<Player> orderedPlayers = ImmutableList.of();

    /**
     * @return Ordered list of players,
     * starting with Small-Blind, ending with Dealer
     */
    public ImmutableList<Player> getOrderedPlayers() {
        return orderedPlayers;
    }

    public void incrementDealer() {
        LinkedList<Player> list = Lists.newLinkedList(orderedPlayers);
        list.addFirst(list.removeLast());
        orderedPlayers = ImmutableList.copyOf(list);
    }

    public void removePlayer(Player player) {
        Validatec.contains(orderedPlayers, player);
        orderedPlayers = ImmutableList.copyOf(
                orderedPlayers.stream().filter(p -> !p.equals(player)).collect(Collectors.toList()));
        Validatec.doesNotContain(orderedPlayers, player);
    }

    public void addPlayer(Player player) {
        Validatec.doesNotContain(orderedPlayers, player);
        orderedPlayers = ImmutableList.<Player>builder().addAll(orderedPlayers).add(player).build();
        player.withTable(this);
    }
}

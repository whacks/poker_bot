package com.rajivprab.poker.GameClient;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import com.rajivprab.poker.GameClient.Card.Rank;
import com.rajivprab.poker.GameClient.Card.Suit;
import com.rajivprab.poker.GameClient.Utils.PotUtils;
import org.rajivprab.cava.Validatec;

import java.util.*;

/**
 * Representation of a single round of Poker
 *
 * TODO: Side-pots not implemented. Someone with small stack who bet a small amount, can win a much bigger pot
 * TODO: If multiple winners, one is chosen randomly
 *
 * Created by rprabhakar on 5/4/15.
 */
public class Round {
    private static final Random RNG = new Random();

    private final Game game;
    private final Queue<Card> deck;
    private final List<Card> communityCards = Lists.newArrayList();
    private final MovesThisHand movesThisHand = new MovesThisHand(Collections.unmodifiableList(communityCards));

    private Street currentStreet;
    private List<Player> activePlayers;

    public Round(Game game) {
        this(game, getNewShuffledDeck());
    }

    public Round(Game game, Queue<Card> deck) {
        this.game = game;
        this.deck = deck;
    }

    public void playRound() {
        setupRound();
        if (activePlayers.size() > 1) { playPreFlop(); }
        if (activePlayers.size() > 1) { playFlop(); }
        if (activePlayers.size() > 1) { playTurn(); }
        if (activePlayers.size() > 1) { playRiver(); }
        resolveRound();
        informPlayersOfAction();
    }

    // ----

    private void setupRound() {
        game.getTable().getOrderedPlayers().stream()
            .filter(p -> p.getStack() <= game.getBigBlind())    // Hack: Anyone with less than the big-blind is removed
            .forEach(game::removePlayer);
        activePlayers = Lists.newLinkedList(game.getTable().getOrderedPlayers());
        activePlayers.forEach(p -> p.dealCards(deck.poll(), deck.poll()));
    }

    private void informPlayersOfAction() {
        game.getTable().getOrderedPlayers().forEach(p -> p.notifyAction(movesThisHand));
        movesThisHand.printAction();
    }

    private void playPreFlop() {
            currentStreet = Street.PRE_FLOP;
            postBlind(0);
            postBlind(1);
            playStreet(getPlayerOrderWithBlindsAtEnd());
    }

    private void postBlind(int playerIndex) {
        int blind = playerIndex == 0 ? game.getSmallBlind() : game.getBigBlind();
        movesThisHand.addMove(activePlayers.get(playerIndex).postBlind(blind), Street.PRE_FLOP);
    }

    private Queue<Player> getPlayerOrderWithBlindsAtEnd() {
        Queue<Player> playerOrder = Lists.newLinkedList(activePlayers);
        playerOrder.add(playerOrder.poll());
        playerOrder.add(playerOrder.poll());
        return playerOrder;
    }

    private void playFlop() {
        currentStreet = Street.FLOP;
        revealCommunityCard();
        revealCommunityCard();
        revealCommunityCard();
        playStreet();
    }

    private void playTurn() {
        currentStreet = Street.TURN;
        revealCommunityCard();
        playStreet();
    }

    private void playRiver() {
        currentStreet = Street.RIVER;
        revealCommunityCard();
        playStreet();
    }

    private void resolveRound() {
        Validatec.notEmpty(activePlayers, "Moves: " + movesThisHand);
        Player winner = activePlayers.size() > 1 ? showdown(activePlayers, communityCards) : activePlayers.get(0);
        winner.collectWinnings(movesThisHand.getPotValue());
    }

    private Player showdown(Collection<Player> players, Collection<Card> communityCards) {
        List<Player> winners = Lists.newArrayList();
        HandStrength winnerStrength = HandStrength.MIN_STRENGTH;
        for (Player player : players) {
            HandStrength curStrength = player.getHandStrength(communityCards);
            PotUtils.printRoundInfo("Showdown! " + player + " has " + curStrength);
            if (curStrength.compareTo(winnerStrength) > 0) {
                winners.clear();
                winners.add(player);
                winnerStrength = curStrength;
            } else if (curStrength.compareTo(winnerStrength) == 0) {
                winners.add(player);
            }
        }

        PotUtils.printRoundInfo("Winners: " + winners + ", HandStrength: " + winnerStrength);
        return winners.get(RNG.nextInt(winners.size()));
    }

    private void revealCommunityCard() {
        Card card = deck.poll();
        communityCards.add(card);
        PotUtils.printRoundInfo("New card shown: " + card);
    }

    private void playStreet() {
        playStreet(Lists.newLinkedList(activePlayers));
    }

    private void playStreet(Queue<Player> playersToAct) {
        Truth.assertThat(playersToAct).containsExactlyElementsIn(activePlayers);
        List<Player> playersWhoHaveActed = Lists.newLinkedList();
        while (!playersToAct.isEmpty()) {
            Player curPlayer = playersToAct.poll();
            if (playersToAct.isEmpty() && playersWhoHaveActed.isEmpty()) { return; }
            Move move = curPlayer.getMove(movesThisHand, currentStreet);
            movesThisHand.addMove(move, currentStreet);
            switch (move.getDecision()) {
                case RAISE:
                    playersToAct.addAll(playersWhoHaveActed);
                    playersWhoHaveActed = Lists.newArrayList(curPlayer);
                    break;
                case CALL:
                case CHECK:
                    playersWhoHaveActed.add(curPlayer);
                    break;
                case FOLD:
                    activePlayers.remove(curPlayer);
                    break;
                default:
                    throw new IllegalStateException("Unknown move: " + move);
            }
        }
        Truth.assertThat(playersWhoHaveActed).containsExactlyElementsIn(activePlayers);
    }

    private static Queue<Card> getNewShuffledDeck() {
        LinkedList<Card> deck = Lists.newLinkedList();
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                deck.add(new Card(suit, rank));
            }
        }
        Collections.shuffle(deck);
        return deck;
    }

    public enum Street { PRE_FLOP, FLOP, TURN, RIVER }
}

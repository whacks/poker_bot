package com.rajivprab.poker.GameClient.Utils;

import com.google.common.collect.Maps;
import com.rajivprab.poker.GameClient.Move;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by rprabhakar on 5/25/15.
 */
public class PotUtils {
    public static Map<String, Integer> getTotalPlayerBets(List<Move> movesThisStreet) {
        Map<String, Integer> playerBets = Maps.newHashMap();
        movesThisStreet.forEach(move -> {
            String curPlayer = move.getPlayerName();
            Integer currentSum = playerBets.get(curPlayer);
            playerBets.put(curPlayer, currentSum == null ? move.getAmount() : currentSum + move.getAmount());
        });
        return playerBets;
    }

    public static int getOtherPlayersHighestBet(Map<String, Integer> playerBets, String myName) {
        int highest = 0;
        for (String playerName : playerBets.keySet()) {
            int aggregateBetAmount = playerBets.get(playerName);
            if (aggregateBetAmount > highest && !playerName.equals(myName)) {
                highest = aggregateBetAmount;
            }
        }
        return highest;
    }

    // 0: small blind
    // num-players - 1: dealer
    public static int getNumPositionsAfterDealer(List<Entry<String, Integer>> playerOrder, String playerName) {
        int numPosition = 0;
        for (Entry<String, Integer> player : playerOrder) {
            if (player.getKey().equals(playerName)) {
                return numPosition;
            } else {
                numPosition++;
            }
        }
        throw new IllegalArgumentException("Player not found: " + playerName);
    }

    // 0: small blind
    // 100: dealer
    public static double getPositionPercentile(List<Entry<String, Integer>> playerOrder, String playerName) {
        return (double) getNumPositionsAfterDealer(playerOrder, playerName) / playerOrder.size();
    }

    public static int getPlayerAggregateBet(Map<String, Integer> playerBets, String playerName) {
        Integer aggregateBet = playerBets.get(playerName);
        return aggregateBet == null ? 0 : aggregateBet;
    }

    public static void printGameEvents(String print) {
        System.out.println(print);
    }

    public static void printErrors(String print) {
        System.out.println(print);
    }

    public static void printRoundInfo(String print) {
        System.out.println(print);
    }
}

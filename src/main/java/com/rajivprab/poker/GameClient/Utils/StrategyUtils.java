package com.rajivprab.poker.GameClient.Utils;

import com.google.common.collect.Lists;
import com.rajivprab.poker.GameClient.*;
import com.rajivprab.poker.GameClient.Card.Rank;
import com.rajivprab.poker.GameClient.HandStrength.Strength;
import com.rajivprab.poker.GameClient.Move.Decision;
import com.rajivprab.poker.GameClient.Round.Street;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Common utilities used for general player strategies
 *
 * Created by rprabhakar on 6/4/15.
 */
public class StrategyUtils {
    /**
     * @param myHoleCards: hole cards
     * @return Sklansky rank indicating the quality of the starting hand
     * Rank 1-4: Playable in any position
     * Rank 5: Playable in middle/late position, or early-position if loose game
     * Rank 6: Playable in late position, or middle-position if loose game
     * Rank 7: Playable only in late position if loose game
     * Rank 8+: Not playable
     */
    public static int getSklanskyRank(Collection<Card> myHoleCards) {
        List<Card> holeCards = Lists.newArrayList(myHoleCards);
        Collections.sort(holeCards, Collections.reverseOrder());
        if (holeCards.get(0).getRank().equals(holeCards.get(1).getRank())) {
            return evaluateHoleCardsPair(holeCards.get(0).getRank());
        } else if (holeCards.get(0).getSuit().equals(holeCards.get(1).getSuit())) {
            return evaluateHoleCardsSuited(holeCards.get(0).getRank(), holeCards.get(1).getRank());
        } else {
            return evaluateDistinctHoleCards(holeCards.get(0).getRank(), holeCards.get(1).getRank());
        }
    }

    public static boolean isTopPair(Collection<Card> communityCards, HandStrength strength) {
        if (!strength.getStrength().equals(Strength.PAIR)) {
            return false;
        }
        Rank pairRank = strength.getKickers().get(0).getRank();
        Rank topCommunalCardRank = getOrderedCards(communityCards).get(0);
        return pairRank.compareTo(topCommunalCardRank) >= 0;
    }

    // Returns all ranks, ordered from highest to lowest
    private static List<Rank> getOrderedCards(Collection<Card> cards) {
        List<Rank> ranks = cards.stream().map(Card::getRank)
                    .collect(Collectors.toSet())
                    .stream().collect(Collectors.toList());
        Collections.sort(ranks);
        Collections.reverse(ranks);
        return ranks;
    }

    public static Move getTightPostFlopMove(Player player, MovesThisHand moves, Street street) {
        HandStrength strength = player.getHandStrength(moves.getCommunityCards());
        int amountToCall = moves.amountToCall(street, player);
        int potValue = moves.getPotValue();
        if (strength.getStrength().compareTo(Strength.PAIR) > 0) {
            int betAmount = amountToCall == 0 ? potValue * 3 / 4 : amountToCall * 2;
            return new Move(player, Decision.RAISE, betAmount).truncateBetIfShort(amountToCall);
        } else if (isTopPair(moves.getCommunityCards(), strength)) {
            return amountToCall > 0 ?
                    new Move(player, Decision.CALL, amountToCall) :
                    new Move(player, Decision.RAISE, potValue * 3 / 4).truncateBetIfShort(amountToCall);
        } else {
            return amountToCall > 0 ? new Move(player, Decision.FOLD) : new Move(player, Decision.CHECK);
        }
    }

    public static Move getTightPreFlopMove(Player player, MovesThisHand moves) {
        return getTightPreFlopMoveHelper(player, moves).truncateBetIfShort(moves.amountToCall(Street.PRE_FLOP, player));
    }

    private static Move getTightPreFlopMoveHelper(Player player, MovesThisHand moves) {
        int sklanskyRank = StrategyUtils.getSklanskyRank(player.getHoleCards());
        int amountToCall = moves.amountToCall(Street.PRE_FLOP, player);
        if (sklanskyRank <= 5) {
            int bigBlind = moves.getBigBlind();
            if (amountToCall < 5 * bigBlind) {
                // No one has bet big so far. Make the first big bet
                return new Move(player, Decision.RAISE, 10 * bigBlind);
            } else if (sklanskyRank <= 1) {
                // Someone else has bet big. Raise further
                int potValue = moves.getPotValue();
                return new Move(player, Decision.RAISE, potValue);
            } else {
                // Someone else has bet big. Call him
                return new Move(player, Decision.CALL, amountToCall);
            }
        } else if (amountToCall <= 0) {
            return new Move(player, Decision.CHECK);
        } else {
            return new Move(player, Decision.FOLD);
        }
    }

    private static int evaluateDistinctHoleCards(Rank highRank, Rank lowRank) {
        Validatec.notEquals(highRank, lowRank);
        if (highRank.equals(Rank.ACE)) {
            switch (lowRank) {
                case KING: return 2;
                case QUEEN: return 3;
                case JACK: return 4;
                case TEN: return 6;
                case NINE: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.KING)) {
            switch (lowRank) {
                case QUEEN: return 4;
                case JACK: return 5;
                case TEN: return 6;
                case NINE: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.QUEEN)) {
            switch (lowRank) {
                case JACK: return 5;
                case TEN: return 6;
                case NINE: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.JACK)) {
            switch (lowRank) {
                case TEN: return 5;
                case NINE: return 7;
                case EIGHT: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.TEN)) {
            switch (lowRank) {
                case NINE: return 7;
                case EIGHT: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.NINE)) {
            switch (lowRank) {
                case EIGHT: return 7;
                default: return 100;
            }
        } else if (highRank.equals(Rank.EIGHT)) {
            switch (lowRank) {
                case SEVEN: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.SEVEN)) {
            switch (lowRank) {
                case SIX: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.SIX)) {
            switch (lowRank) {
                case FIVE: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.FIVE)) {
            switch (lowRank) {
                case FOUR: return 8;
                default: return 100;
            }
        } else {
            return 100;
        }
    }

    private static int evaluateHoleCardsSuited(Rank highRank, Rank lowRank) {
        Validatec.notEquals(highRank, lowRank);
        if (highRank.equals(Rank.ACE)) {
            switch (lowRank) {
                case KING: return 1;
                case QUEEN: return 2;
                case JACK: return 2;
                case TEN: return 3;
                default: return 5;
            }
        } else if (highRank.equals(Rank.KING)) {
            switch (lowRank) {
                case QUEEN: return 2;
                case JACK: return 3;
                case TEN: return 4;
                case NINE: return 6;
                default: return 7;
            }
        } else if (highRank.equals(Rank.QUEEN)) {
            switch (lowRank) {
                case JACK: return 3;
                case TEN: return 4;
                case NINE: return 5;
                case EIGHT: return 7;
                default: return 100;
            }
        } else if (highRank.equals(Rank.JACK)) {
            switch (lowRank) {
                case TEN: return 3;
                case NINE: return 4;
                case EIGHT: return 6;
                case SEVEN: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.TEN)) {
            switch (lowRank) {
                case NINE: return 4;
                case EIGHT: return 5;
                case SEVEN: return 7;
                default: return 100;
            }
        } else if (highRank.equals(Rank.NINE)) {
            switch (lowRank) {
                case EIGHT: return 4;
                case SEVEN: return 5;
                case SIX: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.EIGHT)) {
            switch (lowRank) {
                case SEVEN: return 5;
                case SIX: return 6;
                case FIVE: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.SEVEN)) {
            switch (lowRank) {
                case SIX: return 5;
                case FIVE: return 6;
                case FOUR: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.SIX)) {
            switch (lowRank) {
                case FIVE: return 7;
                case FOUR: return 7;
                default: return 100;
            }
        } else if (highRank.equals(Rank.FIVE)) {
            switch (lowRank) {
                case FOUR: return 6;
                case THREE: return 7;
                default: return 100;
            }
        } else if (highRank.equals(Rank.FOUR)) {
            switch (lowRank) {
                case THREE: return 7;
                case TWO: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.THREE)) {
            switch (lowRank) {
                case TWO: return 8;
                default: return 100;
            }
        } else if (highRank.equals(Rank.TWO)) {
            switch (lowRank) {
                case JACK: return 3;
                case TEN: return 4;
                case NINE: return 5;
                case EIGHT: return 7;
                default: return 100;
            }
        }

        throw new IllegalStateException("All cases missed. Ranks: " + highRank + lowRank);
    }

    private static int evaluateHoleCardsPair(Rank rank) {
        switch (rank) {
            case ACE: return 1;
            case KING: return 1;
            case QUEEN: return 1;
            case JACK: return 1;
            case TEN: return 2;
            case NINE: return 3;
            case EIGHT: return 4;
            case SEVEN: return 5;
            case SIX: return 6;
            case FIVE: return 6;
            case FOUR: return 7;
            case THREE: return 7;
            case TWO: return 7;
            default: throw new IllegalStateException("Bad Rank: " + rank);
        }
    }
}

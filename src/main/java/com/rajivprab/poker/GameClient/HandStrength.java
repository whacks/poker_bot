package com.rajivprab.poker.GameClient;

import com.rajivprab.poker.GameClient.Card.Rank;
import com.rajivprab.poker.GameClient.Card.Suit;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;

import java.util.*;

/**
 * Class representing the strength of a hand, containing any number of cards
 * Includes both hand-type (eg, flush) as well as kicker info
 *
 * Can be compared with another com.rajivprab.poker.GameClient.HandStrength object to figure out which one wins
 *
 * Created by rprabhakar on 4/5/15.
 */
public class HandStrength implements Comparable<HandStrength> {
    public static final HandStrength MIN_STRENGTH = new HandStrength(Lists.newArrayList(
            new Card(Suit.CLUBS, Rank.TWO),
            new Card(Suit.CLUBS, Rank.THREE),
            new Card(Suit.CLUBS, Rank.FOUR),
            new Card(Suit.HEARTS, Rank.FIVE),
            new Card(Suit.HEARTS, Rank.SEVEN)));

    public enum Strength {
        HIGH_CARD,
        PAIR,
        TWO_PAIR,
        TRIPS,
        STRAIGHT,
        FLUSH,
        FULL_HOUSE,
        FOUR_OF_A_KIND,
        STRAIGHT_FLUSH
    }

    private Strength strength;

    // Ordered list of kickers, such that 2 hands with same strengths. can be compared by kicker, starting from index-0
    // Note that it contains the pair/set cards as well, and not just "kickers"
    // Does not contain any cards unused for HandStrength (eg, 6th and 7th unused cards)
    private List<Card> kickers = Lists.newArrayList();

    public HandStrength(Collection<Card> cards) {
        Validatec.greaterThan(cards.size(), 4, "Cards: " + cards);
        parsePairs(cards);

        List<Card> isStraightFlush = checkForStraightFlush(cards);
        List<Card> isFlush = checkForFlush(cards);
        List<Card> isStraight = checkForStraight(cards);

        if (isStraightFlush != null) {
            strength = Strength.STRAIGHT_FLUSH;
            kickers = isStraightFlush;
        } else if (isFlush != null && strength.compareTo(Strength.FLUSH) < 0) {
            strength = Strength.FLUSH;
            kickers = isFlush;
        } else if (isStraight != null && strength.compareTo(Strength.STRAIGHT) < 0) {
            strength = Strength.STRAIGHT;
            kickers = isStraight;
        }
    }

    @Override
    public int compareTo(HandStrength ref) {
        Validatec.size(getKickers(),5);
        Validatec.size(ref.getKickers(), 5);

        if (this.getStrength().equals(ref.getStrength())) {
            for (int i=0; i<getKickers().size(); i++) {
                Rank myKicker = this.getKickers().get(i).getRank();
                Rank refKicker = ref.getKickers().get(i).getRank();
                if (!myKicker.equals(refKicker)) {
                    return myKicker.compareTo(refKicker);
                }
            }
            return 0;
        } else {
            return strength.compareTo(ref.strength);
        }
    }

    public int getHandPercentile() {
        // TODO
        return 50;
    }

    public boolean bestHandContainsCard(Card ref) {
        return getKickers().contains(ref);
    }

    // ----------

    private void parsePairs(Collection<Card> cards) {
        Map<Rank, Set<Card>> matchedCards = getCardsInSet(cards);

        List<Set<Card>> singles = Lists.newArrayList();
        List<Set<Card>> pairs = Lists.newArrayList();
        List<Set<Card>> trips = Lists.newArrayList();
        List<Set<Card>> quads = Lists.newArrayList();
        populateSets(singles, pairs, trips, quads, matchedCards);

        Validate.isTrue(quads.size()*4 +
                        trips.size()*3 +
                        pairs.size()*2 +
                        singles.size()
                        == cards.size(), "All cards should be accounted for");

        updateHandStrength(pairs, trips, quads, cards);
    }

    private static Map<Rank, Set<Card>> getCardsInSet(Collection<Card> cards) {
        Map<Rank, Set<Card>> matchedCards = Maps.newEnumMap(Rank.class);
        for (Card card : cards) {
            Set<Card> mySet = matchedCards.get(card.getRank());
            if (mySet == null) {
                mySet = Sets.newHashSet(card);
                matchedCards.put(card.getRank(), mySet);
            } else {
                mySet.add(card);
            }
        }

        return matchedCards;
    }

    private static void populateSets(List<Set<Card>> singles, List<Set<Card>> pairs,
                                     List<Set<Card>> trips, List<Set<Card>> quads,
                                     Map<Rank, Set<Card>> matchedCards) {
        for (int i = Rank.values().length - 1; i >= 0; i--) {
            Rank curRank = Rank.values()[i];
            Set<Card> curCards = matchedCards.get(curRank);
            if (curCards != null) {
                int count = curCards.size();
                Validate.inclusiveBetween(1, 4, count, "Cannot have more than 4 of a kind");

                if (count == 1) {
                    singles.add(curCards);
                } else if (count == 2) {
                    pairs.add(curCards);
                } else if (count == 3) {
                    trips.add(curCards);
                } else if (count == 4) {
                    quads.add(curCards);
                }
            }
        }
    }

    private void updateHandStrength(List<Set<Card>> pairs, List<Set<Card>> trips,
                                    List<Set<Card>> quads, Collection<Card> cards) {
        List<Card> highCards = Lists.newArrayList(cards);
        if (quads.size() >= 1) {
            strength = Strength.FOUR_OF_A_KIND;
            kickers.addAll(quads.get(0));
        } else if (trips.size() >= 2) {
            strength = Strength.FULL_HOUSE;
            kickers.addAll(trips.get(0));
            kickers.addAll(trips.get(1));
        } else if (trips.size() >= 1 && pairs.size() >= 1) {
            strength = Strength.FULL_HOUSE;
            kickers.addAll(trips.get(0));
            kickers.addAll(pairs.get(0));
        } else if (trips.size() >= 1) {
            strength = Strength.TRIPS;
            kickers.addAll(trips.get(0));
        } else if (pairs.size() >= 2) {
            strength = Strength.TWO_PAIR;
            kickers.addAll(pairs.get(0));
            kickers.addAll(pairs.get(1));
        } else if (pairs.size() >= 1) {
            strength = Strength.PAIR;
            kickers.addAll(pairs.get(0));
        } else {
            strength = Strength.HIGH_CARD;
        }
        highCards.removeAll(kickers);
        kickers.addAll(getCardsByDescendingRank(highCards));
        Validatec.greaterThan(kickers.size(), 4, "Kickers: " + kickers);
        kickers = kickers.subList(0, 5);
    }

    private static List<Card> checkForStraightFlush(Collection<Card> cards) {
        Collection<Card> flushCards = checkForFlush(cards);
        return flushCards == null ? null : checkForStraight(flushCards);
    }

    /**
     * @param cards to check against
     * @return If flush exists, list of all flush-cards in descending order
     * If more than 5 cards match a suit, all of them will be returned
     * Caller has to prune the list before inserting into kicker
     */
    private static List<Card> checkForFlush(Collection<Card> cards) {
        for (Suit suit : Suit.values()) {
            List<Card> matchingCards = getCardsMatchingSuit(cards, suit);
            if (matchingCards.size() >= 5) {
                return getCardsByDescendingRank(matchingCards).subList(0, 5);
            }
        }
        return null;
    }

    /**
     * @param cards to check against
     * @return If straight exists, list of straight-cards in descending order
     * If no straight exists, returns null
     * Note: Only the top-5 cards will be returned
     * Cards with same rank but different suit, will be ignored
     */
    private static List<Card> checkForStraight(Collection<Card> cards) {
        List<Card> cardsCopy = getCardsByDescendingRank(cards);
        Iterator<Card> itr = cardsCopy.iterator();
        int prevRank = Integer.MIN_VALUE;
        List<Card> chain = Lists.newArrayList();

        while (itr.hasNext()) {
            Card newCard = itr.next();
            int curRank = newCard.getRank().ordinal();

            if (curRank + 1 == prevRank) {
                chain.add(newCard);
            } else if (curRank != prevRank) {
                chain.clear();
                chain.add(newCard);
            }

            prevRank = curRank;
            if (chain.size() == 5) {
                return chain;
            }
        }
        return null;
    }

    private static List<Card> getCardsMatchingSuit(Collection<Card> cards, Suit matchSuit) {
        List<Card> result = Lists.newArrayList();
        for (Card card : cards) {
            if (card.getSuit().equals(matchSuit)) {
                result.add(card);
            }
        }
        return result;
    }

    private static List<Card> getCardsByDescendingRank(Collection<Card> cards) {
        List<Card> result = Lists.newArrayList(cards);
        result.sort(Collections.reverseOrder());
        return result;
    }

    public Strength getStrength() {
        return strength;
    }

    public List<Card> getKickers() {
        return Collections.unmodifiableList(kickers);
    }

    @Override
    public String toString() {
        return "Strength: " + strength.name() + ", Kickers: " + getKickers();
    }
}

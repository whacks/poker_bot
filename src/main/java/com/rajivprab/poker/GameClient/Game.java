package com.rajivprab.poker.GameClient;

import com.rajivprab.poker.GameClient.Utils.PotUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Scanner;

/**
 * Representation of a com.rajivprab.poker.GameClient.Game state
 *
 * Created by rprabhakar on 4/5/15.
 */
public class Game {
    private static final Log log = LogFactory.getLog(Game.class);

    private Table table = new Table();
    private int smallBlind;
    private int bigBlind;

    public Game(int smallBlind, int bigBlind) {
        Validate.isTrue(smallBlind < bigBlind, "Small blind should be less than big blind");
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
    }

    public Game withPlayers(Collection<Player> players) {
        for (Player player : players) {
            withPlayer(player);
        }
        return this;
    }

    public Game withPlayer(Player player) {
        System.out.println("Adding player: " + player + ", of class: " + player.getClass());
        table.addPlayer(player);
        return this;
    }

    public Table getTable() {
        return table;
    }

    public void removePlayer(Player player) {
        table.removePlayer(player);
        PotUtils.printGameEvents("Player is out! " + player);
    }

    public int getSmallBlind() {
        return smallBlind;
    }

    public int getBigBlind() {
        return bigBlind;
    }

    public void playRounds(int numRounds) {
        for (int i=0; i<numRounds; i++) {
            if (table.getOrderedPlayers().size() == 1) { break; }
            table.incrementDealer();
            new Round(this).playRound();
        }

        System.out.println("Players Left Standing \n-----------");
        for (Player player: getTable().getOrderedPlayers()) {
            String line = String.format("Player: %s, Class: %s, Final stack: %d",
                    player, player.getClass().getName(), player.getStack());
            PotUtils.printGameEvents(line);
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner reader = new Scanner(System.in);

        System.out.print("Enter Small Blind: ");
        int smallBlind = reader.nextInt();

        System.out.print("Enter Big Blind: ");
        int bigBlind = reader.nextInt();

        System.out.print("Enter Starting stack: ");
        int startingStack = reader.nextInt();

        System.out.print("Enter Num Players: ");
        int numPlayers = reader.nextInt();
        Validate.inclusiveBetween(2, 10, numPlayers, "Num players has to be within [2, 10]");

        System.out.print("Enter Num Rounds to Play: ");
        int numRounds = reader.nextInt();

        Game game = new Game(smallBlind, bigBlind);

        for (int i=0; i<numPlayers; i++) {
            System.out.print("Enter player " + i + " name: ");
            String playerName = reader.next();

            System.out.print("Enter player class name: ");
            String playerClass = reader.next();

            try {
                Player player = (Player) Class.forName(playerClass)
                        .getDeclaredConstructor(String.class, Integer.class)
                        .newInstance(playerName, startingStack);
                game.withPlayer(player);
            } catch (Exception e) {
                System.out.print("Error: Class not found. Deleting this player");
                throw e;
            }
        }

        game.playRounds(numRounds);
    }
}
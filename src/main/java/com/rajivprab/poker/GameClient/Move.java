package com.rajivprab.poker.GameClient;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.Validate;

/**
 * Represents a single action/move made by a player
 *
 * Created by rprabhakar on 4/5/15.
 */
public class Move {
    public enum Decision { FOLD, CHECK, CALL, RAISE }

    private final Player player;
    private final Decision decision;
    private final int amount;

    public Move(Player player, Decision decision) {
        this(player, decision, 0);
    }

    public Move(Player player, Decision decision, int amount) {
        Validate.isTrue((Lists.newArrayList(Decision.FOLD, Decision.CHECK).contains(decision) && amount == 0) ||
                                (Lists.newArrayList(Decision.CALL, Decision.RAISE).contains(decision) && amount > 0));
        this.player = player;
        this.decision = decision;
        this.amount = amount;
    }

    public Decision getDecision() {
        return decision;
    }

    public int getAmount() {
        return amount;
    }

    public String getPlayerName() { return player.getName(); }

    // Returns a new object, does not modify this one
    public Move truncateBetIfShort(int amountToCall) {
        if (!getDecision().equals(Decision.RAISE) || player.getStack() >= getAmount()) {
            return this;
        }
        return player.getStack() > amountToCall ?
                new Move(player, Decision.RAISE, player.getStack()) :
                new Move(player, Decision.CALL, amountToCall);
    }

    @Override
    public String toString() {
        return player.getName() + " " + decision.toString() + " for $" + amount;
    }
}

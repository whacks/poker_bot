package poker.GameClient;

import com.rajivprab.poker.GameClient.Game;
import com.rajivprab.poker.GameClient.Player;
import com.rajivprab.poker.PlayerImpl.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;
import java.util.stream.IntStream;


/**
 * Integration test for entire game
 *
 * Created by rprabhakar on 5/25/15.
 */
public class GameTest {
    @Test
    public void callingStations() {
        runGame(new CallingStationFactory(), 10, 200);
    }

    @Test
    public void randomPlayers() {
        runGame(new RandomPlayerFactory(), 10, 200);
    }

    @Test
    public void mixedPlayers() {
        runLeague(new MixedFactory(), 10, 200);
    }

    @Ignore
    public void ruleBreakers() {
        runGame(new RuleBreakerFactory(), 10, 200);
    }

    @Test
    public void tightPreFlopPlayers() {
        runGame(new TightPreFlopFactory(), 10, 200);
    }

    private void runLeague(PlayerFactory factory, int numPlayers, int buyIn) {
        IntStream.range(0, 100).forEach(i -> runGame(factory, numPlayers, buyIn));
    }

    private void runGame(PlayerFactory factory, int numPlayers, int buyIn) {
        Game game = new Game(1, 2);
        IntStream.range(0, numPlayers).forEach(i -> game.withPlayer(factory.getPlayer(buyIn)));
        game.playRounds(2000);
    }

    private interface PlayerFactory {
        Player getPlayer(int buyIn);
    }

    /*
    TODO How to make this work?
    private static class GenericPlayerFactory<PlayerClass> implements PlayerFactory {
        @Override
        public Player getPlayer(int buyIn) {
            return PlayerClass.class
                    .getDeclaredConstructor(String.class, Integer.class)
                    .newInstance(RandomStringUtils.randomAlphabetic(10), buyIn);
        }
    }
    */

    private static class TightPreFlopFactory implements PlayerFactory {
        @Override
        public Player getPlayer(int buyIn) {
            return new TightPreFlopPlayer(RandomStringUtils.randomAlphabetic(10), buyIn);
        }
    }

    private static class CallingStationFactory implements PlayerFactory {
        @Override
        public Player getPlayer(int buyIn) {
            return new CallingStation(RandomStringUtils.randomAlphabetic(10), buyIn);
        }
    }

    private static class RandomPlayerFactory implements PlayerFactory {
        @Override
        public Player getPlayer(int buyIn) {
            return new RandomPlayer(RandomStringUtils.randomAlphabetic(10), buyIn);
        }
    }

    private static class RuleBreakerFactory implements PlayerFactory {
        @Override
        public Player getPlayer(int buyIn) {
            return new RuleBreaker(RandomStringUtils.randomAlphabetic(10), buyIn);
        }
    }

    private static class MixedFactory implements PlayerFactory {
        private static final Random RNG = new Random();

        @Override
        public Player getPlayer(int buyIn) {
            int random = RNG.nextInt(110);
            if (random < 25) {
                return new CallingStation(RandomStringUtils.randomAlphabetic(10), buyIn);
            } else if (random < 50) {
                return new TightPreFlopPlayer(RandomStringUtils.randomAlphabetic(10), buyIn);
            } else if (random < 75) {
                return new RandomPlayer(RandomStringUtils.randomAlphabetic(10), buyIn);
            } else if (random < 100) {
                return new TightPlayer(RandomStringUtils.randomAlphabetic(10), buyIn);
            } else {
                return new RuleBreaker(RandomStringUtils.randomAlphabetic(10), buyIn);
            }
        }
    }
}

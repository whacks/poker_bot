package poker.GameClient.Utils;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import com.rajivprab.poker.GameClient.Card;
import com.rajivprab.poker.GameClient.Card.Rank;
import com.rajivprab.poker.GameClient.Card.Suit;
import com.rajivprab.poker.GameClient.HandStrength;
import com.rajivprab.poker.GameClient.Utils.StrategyUtils;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Unit tests
 * <p>
 * Created by rajivprab on 2/18/17.
 */
public class StrategyUtilsTest {
    @Test
    public void isSklanskyRankOne() {
        // TODO
    }

    @Test
    public void isSklanskyRankEight() {
        // TODO
    }

    @Test
    public void sklanskyRank_isSuited() {
        // TODO
    }

    @Test
    public void sklanskyRank_isPaired() {
        // TODO
    }

    // --------

    @Test
    public void preFlopShouldRaise() {
        // TODO
    }

    @Test
    public void preFlopShouldCall() {
        // TODO
    }

    @Test
    public void preFlopShouldFold() {
        // TODO
    }

    // --------

    @Test
    public void postFlopShouldRaise() {
        // TODO
    }

    @Test
    public void postFlopShouldCall() {
        // TODO
    }

    @Test
    public void postFlopShouldFold() {
        // TODO
    }

    // --------

    @Test
    public void isTopPair() {
        runTest(
                ImmutableList.of(
                        new Card(Suit.DIAMONDS, Rank.SEVEN),
                        new Card(Suit.SPADES, Rank.SIX),
                        new Card(Suit.CLUBS, Rank.FOUR)),
                ImmutableList.of(
                        new Card(Suit.HEARTS, Rank.ACE),
                        new Card(Suit.CLUBS, Rank.SEVEN)),
                true);

    }

    @Test
    public void isOverPair() {
        runTest(
                ImmutableList.of(
                        new Card(Suit.DIAMONDS, Rank.SEVEN),
                        new Card(Suit.SPADES, Rank.SIX),
                        new Card(Suit.CLUBS, Rank.FOUR)),
                ImmutableList.of(
                        new Card(Suit.HEARTS, Rank.ACE),
                        new Card(Suit.CLUBS, Rank.ACE)),
                true);
    }

    @Test
    public void isMidPair() {
        runTest(
                ImmutableList.of(
                        new Card(Suit.DIAMONDS, Rank.SEVEN),
                        new Card(Suit.SPADES, Rank.SIX),
                        new Card(Suit.CLUBS, Rank.FOUR)),
                ImmutableList.of(
                        new Card(Suit.HEARTS, Rank.ACE),
                        new Card(Suit.CLUBS, Rank.SIX)),
                false);
    }

    @Test
    public void isBottomPair() {
        runTest(
                ImmutableList.of(
                        new Card(Suit.DIAMONDS, Rank.SEVEN),
                        new Card(Suit.SPADES, Rank.SIX),
                        new Card(Suit.CLUBS, Rank.FOUR)),
                ImmutableList.of(
                        new Card(Suit.HEARTS, Rank.ACE),
                        new Card(Suit.SPADES, Rank.FOUR)),
                false);
    }

    @Test
    public void isHighCard() {
        runTest(
                ImmutableList.of(
                        new Card(Suit.DIAMONDS, Rank.SEVEN),
                        new Card(Suit.SPADES, Rank.SIX),
                        new Card(Suit.CLUBS, Rank.FOUR)),
                ImmutableList.of(
                        new Card(Suit.HEARTS, Rank.ACE),
                        new Card(Suit.CLUBS, Rank.KING)),
                false);
    }

    @Test
    public void isTrips() {
        runTest(
                ImmutableList.of(
                        new Card(Suit.DIAMONDS, Rank.SEVEN),
                        new Card(Suit.SPADES, Rank.SIX),
                        new Card(Suit.CLUBS, Rank.FOUR)),
                ImmutableList.of(
                        new Card(Suit.HEARTS, Rank.SEVEN),
                        new Card(Suit.CLUBS, Rank.SEVEN)),
                false);
    }

    private static void runTest(List<Card> communityCards, List<Card> holeCards, boolean isTopPair) {
        Truth.assertThat(communityCards).containsNoneIn(holeCards);
        HandStrength strength = new HandStrength(Stream.concat(communityCards.stream(), holeCards.stream())
                                                       .collect(Collectors.toList()));
        Truth.assertThat(StrategyUtils.isTopPair(communityCards, strength)).isEqualTo(isTopPair);
    }
}

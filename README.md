Platform that allows you to build your own automated-poker-bot, and test it out against other implementations to see how well it does.

To do so:

1. Add your implementation class to the PlayerImpl package.
2. Add your class to the GameTest.mixedFactory.
3. Run GameTest.mixedPlayers(). See the logs to see how well you did.

Structure:

com.rajivprab.poker.GameClient.Game
------
Contains:
- all players
- central deck
- open cards
- central pot
- betting pot
- dealer button


com.rajivprab.poker.GameClient.Card
-----
- number
- suit


com.rajivprab.poker.GameClient.Player
-------
- name
- hand
- chips
- AI/human


com.rajivprab.poker.GameClient.Move
------
- fold/call/raise/check
- amount to raise


com.rajivprab.poker.GameClient.Hand
-----
- Set of cards
- Strength of the hand


com.rajivprab.poker.GameClient.HandStrength
------------
- Value of current hand (pair, flush, straight, etc)
- All cards arranged in list, for kicker-comparison


TODOs
-------
- Side-pots. Currently, players can "borrow money" and go negative until end of hand

Unique Features
----------------
- Side pots eliminated. If players' stack goes to 0, they can continue calling until the end of the round.
- Players will be eliminated from the game whenever new cards are dealt, if they have a negative/zero stack
- 1000ms timeout on computeMove() and informOfAction() method calls. Penalty for exceeding: Fold + 10% of stack lost
- If there's a tie, winner decided by random coin-flip
- Min-raise: Any amount desired